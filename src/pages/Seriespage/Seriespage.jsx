import React from 'react'

import Header from '../../components/Header/Header';
import MovieItem from '../../components/MovieItem/MovieItem';

import './Seriespage.css';
const Seriespage = ({movies, title}) => {
    const yearFilter = 2010;
    const maxItems = 21;
    const filteredMovies = movies?.filter((movie) => movie.programType === title && movie.releaseYear >= yearFilter);
    const sortedMovies = filteredMovies.sort((a, b) => a.title > b.title ? 1 : -1);
    return (
        <div id="series">
            <Header title="series"/>
            <div className="wrapper">
                {sortedMovies?.map((item, idx) => {
                    while(idx < maxItems ){
                        return <MovieItem movie={item} key={idx}/>
                    }
                    return null;
                })}
            </div>
        </div>
    )
}

export default Seriespage;
