import React from 'react'
import Header from '../../components/Header/Header';
import Homeitem from '../../components/Homeitem/Homeitem';
import Footer from '../../components/Footer/Footer';

import './Homepage.css'
const Homepage = ({title, loading, error}) =>  {
    return (
        <div>
            <Header title={title}/>
            <div className="wrapper">
                {loading ? <p>Loading</p> : <></>}
                {error ? <p>Ooops, something went wrong</p> : <></>}
                <Homeitem title="series" />
                <Homeitem title="movies" />
            </div>
            <Footer />
        </div>
    )
}

export default Homepage;
