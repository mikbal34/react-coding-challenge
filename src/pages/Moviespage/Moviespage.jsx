import React from 'react'

import Header from '../../components/Header/Header';
import MovieItem from '../../components/MovieItem/MovieItem';

import './Moviespage.css';
function Moviespage({movies, title}) {
    const yearFilter = 2010;
    const maxItems = 21;
    const filteredMovies = movies?.filter((movie) => movie.programType === "movie" && movie.releaseYear >= yearFilter);
    const sortedMovies = filteredMovies.sort((a, b) => a.title > b.title ? 1 : -1);
    return (
        <div id="movies">
            <Header title="movies"/>
            <div className="wrapper">
                {sortedMovies?.map((item, idx) => {
                    while(idx < maxItems){
                        return <MovieItem movie={item} key={idx}/>
                    }
                    return null;
                })}
            </div>
        </div>
    )
}

export default Moviespage;
