import React from 'react';
import { useHistory } from 'react-router-dom';
import placeholder from '../../assets/placeholder.png';


import './Homeitem.css';
const Homeitem = ({title}) => {
    const history = useHistory();

    const handleClick = (title) => history.push(`/${title.toLowerCase()}`)
    return (
        <div onClick={() => handleClick(title)} className="home-item">
           <div className="overlay">
                <h2>{title}</h2>
                <img src={placeholder} alt="homeitem"/>
           </div>
            <p>Popular {title}</p>
        </div>
    );
}

export default Homeitem;
