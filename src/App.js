import {useEffect, useState} from 'react';
import {Switch, BrowserRouter as Router, Route } from 'react-router-dom';
import axios from 'axios';
import Home from './pages/Homepage/Homepage';
import Series from './pages/Seriespage/Seriespage';
import Movies from './pages/Moviespage/Moviespage';
import './App.css';

function App() {
  const [movies, setMovies] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    setLoading(true);
    axios.get("https://raw.githubusercontent.com/StreamCo/react-coding-challenge/master/feed/sample.json")
    .then((res) => {
      setMovies(res.data.entries);
      setLoading(false);
    })
    .catch((err) => setError(err.message));
  }, []);

  return (    
    <Router>
      <Switch>
        <Route exact path="/" render={(props) => <Home {...props} loading={loading} error={error} title="Titles" />} />
        <Route exact path="/series" render={(props) => <Series {...props} title="series" movies={movies} />} />
        <Route exact path="/movies" render={(props) => <Movies {...props} title="movies" movies={movies} />} />
      </Switch>
    </Router>
  );
}

export default App;